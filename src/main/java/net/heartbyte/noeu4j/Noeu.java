package net.heartbyte.noeu4j;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import net.heartbyte.noeu4j.net.Client;
import net.heartbyte.noeu4j.net.DefaultClient;
import net.heartbyte.noeu4j.net.Connection;
import net.heartbyte.noeu4j.net.DefaultConnection;
import net.heartbyte.noeu4j.query.Query;
import net.heartbyte.noeu4j.rpc.Command;
import net.heartbyte.noeu4j.rpc.CommandBuilder;
import net.heartbyte.noeu4j.rpc.Result;
import net.heartbyte.noeu4j.util.RandomString;

import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Noeu {
    public static Map<String, Type> types;

    public final Gson       gson;
    public       Client     client;
    public       Connection connection;
    public       String     address;
    public       boolean    websocket;

    static {
        types = new HashMap<>();

        types.put("Map<String,Object>", new TypeToken<Map<String, Object>>(){ }.getType());
        types.put("Result", new TypeToken<Result>(){}.getType());
    }

    public Noeu(Client client, String address) {
        this.gson      = new Gson();
        this.client    = client;
        this.address   = address;
        this.websocket = false;
    }

    public void initializeWebsocket(String url) throws URISyntaxException, InterruptedException {
        this.connection = new DefaultConnection(url, new HashMap<>());
        this.connection.setType(types.get("Result"));
        this.websocket  = true;
    }

    public Result execute(Query query) {
        Command command = Command.fromQuery(query);
        Result  result  = this.execute(command);
        query.result = result;
        return result;
    }

    public Result execute(Command command) {
        Result result;
        Client client;

        CompletableFuture<Result> future;

        if (this.websocket) {
            command.ID = RandomString.getRandomString(32);

            future = this.connection.execute(command);
        } else {
            if (this.client == null)
                client = new DefaultClient();
            else
                client = this.client;

            HashMap<String, List<String>> headers = new HashMap<>();

            headers.put("Content-Type", Arrays.asList("application/json"));

            future = client.Execute(
                    "POST",
                    this.address + "/execute",
                    command.serialize(this.gson),
                    headers,
                    types.get("Result"),
                    true);
        }

        try {
            result = future.get(5, TimeUnit.SECONDS);
        } catch (ExecutionException | InterruptedException | TimeoutException exception) {
            throw new RuntimeException(exception);
        }

        return result;
    }


}