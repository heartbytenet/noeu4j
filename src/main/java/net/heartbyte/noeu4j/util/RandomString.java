package net.heartbyte.noeu4j.util;

import java.util.Random;

public class RandomString {
    public static String hexdigits = "0123456789ABCDEF";

    public static String getRandomString(int length) {
        StringBuilder builder = new StringBuilder();
        Random        random  = new Random(System.currentTimeMillis());

        for (int i = 0; i < length; i++) {
            builder.append(hexdigits.charAt(random.nextInt(hexdigits.length())));
        }

        return builder.toString();
    }
}
