package net.heartbyte.noeu4j.rpc;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import net.heartbyte.noeu4j.query.Query;

import java.nio.charset.StandardCharsets;
import java.util.Map;

public class Command {
    @SerializedName("id")
    public String ID;

    public String              namespace;
    public String              method;
    public Map<String, Object> params;

    public static CommandBuilder builder() {
        return new CommandBuilder();
    }

    public static Command fromQuery(Query query) {
        return Command.builder()
                .namespace(query.namespace)
                .method(query.method)
                .params(query.params)
                .build();
    }

    public byte[] serialize(Gson gson) {
        return gson.toJson(this).getBytes(StandardCharsets.UTF_8);
    }
}