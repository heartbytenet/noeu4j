package net.heartbyte.noeu4j.rpc;

import java.util.HashMap;
import java.util.Map;

public class CommandBuilder {
    String              namespace;
    String              method;
    Map<String, Object> params;

    public CommandBuilder() {
        this.namespace = null;
        this.method    = null;
        this.params    = new HashMap<>();
    }

    public CommandBuilder namespace(String value) {
        if (value != null)
            this.namespace = value;

        return this;
    }

    public CommandBuilder method(String value) {
        if (value != null)
            this.method = value;

        return this;
    }

    public CommandBuilder params(Map<String, Object> value) {
        if (value != null)
            this.params = value;

        return this;
    }

    public CommandBuilder params(String key, Object val) {
        if (this.params == null)
            this.params = new HashMap<>();

        this.params.put(key, val);

        return this;
    }

    public Command build() {
        Command command = new Command();

        command.namespace = this.namespace;
        command.method    = this.method;
        command.params    = this.params;

        return command;
    }
}
