package net.heartbyte.noeu4j.rpc;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class Result {
    @SerializedName("id")
    public String ID;

    public Boolean             success;
    public Map<String, Object> payload;
    public String              error;
}
