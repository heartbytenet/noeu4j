package net.heartbyte.noeu4j.net;

import com.google.gson.Gson;
import net.heartbyte.noeu4j.rpc.Command;
import net.heartbyte.noeu4j.rpc.Result;

import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

public class DefaultConnection implements Connection {
    public final Gson gson = new Gson();

    public ClientWebsocket client;

    public ConcurrentHashMap<String, CompletableFuture<Result>> futures = new ConcurrentHashMap<>();

    public Type type;

    // This is low effort code but I'm tired
    public DefaultConnection(String url, Map<String, String> headers) throws URISyntaxException, InterruptedException {
        this.client = new ClientWebsocket(this, new URI(url), headers);
        this.client.connectBlocking();
    }

    @Override
    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public CompletableFuture<Result> execute(Command command) {
        CompletableFuture<Result> future = new CompletableFuture<>();

        this.client.send(command.serialize(this.gson));
        this.futures.put(command.ID, future);

        return future;
    }
}
