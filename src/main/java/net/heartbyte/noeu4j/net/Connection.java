package net.heartbyte.noeu4j.net;

import net.heartbyte.noeu4j.rpc.Command;
import net.heartbyte.noeu4j.rpc.Result;

import java.lang.reflect.Type;
import java.util.concurrent.CompletableFuture;

public interface Connection {
    void setType(Type type);
    CompletableFuture<Result> execute(Command command);
}
