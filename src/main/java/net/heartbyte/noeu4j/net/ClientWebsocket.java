package net.heartbyte.noeu4j.net;

import net.heartbyte.noeu4j.rpc.Result;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Logger;

public class ClientWebsocket extends WebSocketClient {
    public final DefaultConnection connection;

    public ClientWebsocket(DefaultConnection connection, URI endpoint, Map<String, String> headers) {
        super(endpoint, headers);

        this.connection = connection;
    }

    @Override
    public void onOpen(ServerHandshake data) {
        Logger.getGlobal().info("noeu4j websocket connection open");
    }

    @Override
    public void onMessage(String message) {
        Result result = this.connection.gson.fromJson(message, this.connection.type);
        CompletableFuture<Result> future = this.connection.futures.get(result.ID);
        if (future != null)
            future.complete(result);
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        Logger.getGlobal().info("noeu4j websocket connection closed");
    }

    @Override
    public void onError(Exception exception) {
        exception.printStackTrace(System.out);
    }
}
