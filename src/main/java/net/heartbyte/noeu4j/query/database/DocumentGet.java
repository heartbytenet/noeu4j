package net.heartbyte.noeu4j.query.database;

import net.heartbyte.noeu4j.query.Query;

import java.util.HashMap;

public class DocumentGet extends Query {
    public DocumentGet(String collectionID, String documentID) {
        this.namespace = "database";
        this.method    = "document.get";
        this.params    = new HashMap<>();

        this.params.put("collection.id", collectionID);
        this.params.put("document.id",   documentID);
    }

    @Override
    public Object result() {
        if (this.result == null)
            return null;

        if (!this.result.success)
            return null;

        return this.result.payload.get("document.data");
    }
}
