package net.heartbyte.noeu4j.query.database;

import net.heartbyte.noeu4j.query.Query;

import java.util.HashMap;

public class DocumentDel extends Query {
    public DocumentDel(String collectionID, String documentID) {
        this.namespace = "database";
        this.method    = "document.del";
        this.params    = new HashMap<>();

        this.params.put("collection.id", collectionID);
        this.params.put("document.id",   documentID);
    }

    @Override
    public Boolean result() {
        return this.result.success;
    }
}
