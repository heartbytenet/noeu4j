package net.heartbyte.noeu4j.query.database;

import net.heartbyte.noeu4j.query.Query;

import java.util.HashMap;

public class DocumentSet extends Query {
    public DocumentSet(String collectionID, String documentID, Long documentTTL, Object documentData) {
        this.namespace = "database";
        this.method    = "document.set";
        this.params    = new HashMap<>();

        this.params.put("collection.id", collectionID);
        this.params.put("document.id",   documentID);
        this.params.put("document.ttl",  documentTTL);
        this.params.put("document.data", documentData);
    }
}
