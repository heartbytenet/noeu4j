package net.heartbyte.noeu4j.query.database;

import net.heartbyte.noeu4j.query.Query;

import java.util.HashMap;

public class DocumentFieldSet extends Query {
    public DocumentFieldSet(String collectionID, String documentID, String documentField, Object documentValue) {
        this.namespace = "database";
        this.method    = "document.field.set";
        this.params    = new HashMap<>();

        this.params.put("collection.id",  collectionID);
        this.params.put("document.id",    documentID);
        this.params.put("document.field", documentField);
        this.params.put("document.value", documentValue);
    }

    @Override
    public Object result() {
        if (this.result == null)
            return null;

        if (!this.result.success)
            return null;

        return this.result.payload.get("document.field.value");
    }
}
