package net.heartbyte.noeu4j.query.database;

import net.heartbyte.noeu4j.query.Query;

import java.util.HashMap;
import java.util.List;

public class DocumentListID extends Query {
    public DocumentListID(String collectionID) {
        this.namespace = "database";
        this.method    = "document.list.id";
        this.params    = new HashMap<>();

        this.params.put("collection.id", collectionID);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<String> result() {
        if (this.result == null)
            return null;

        if (!this.result.success)
            return null;

        return (List<String>) this.result.payload.get("document.list.id");
    }
}
