package net.heartbyte.noeu4j.query.database;

import net.heartbyte.noeu4j.query.Query;

import java.util.HashMap;

public class DocumentFieldAdd extends Query {
    public DocumentFieldAdd(String collectionID, String documentID, String documentField, long documentValue) {
        this.namespace = "database";
        this.method    = "document.field.add";
        this.params    = new HashMap<>();

        this.params.put("collection.id",  collectionID);
        this.params.put("document.id",    documentID);
        this.params.put("document.field", documentField);
        this.params.put("document.value", documentValue);
    }

    @Override
    public Long result() {
        if (this.result == null)
            return null;

        if (!this.result.success)
            return null;

        // Json numbers are all doubles by default
        Object raw = this.result.payload.get("document.field.value");
        if (raw instanceof Double) {
            return ((Double) raw).longValue();
        } else if (raw instanceof Long) {
            return (Long) raw;
        }

        return null;
    }
}
