package net.heartbyte.noeu4j.query;

import net.heartbyte.noeu4j.rpc.Result;

import java.util.Map;

public abstract class Query {
    public String              namespace;
    public String              method;
    public Map<String, Object> params;
    public Result              result;

    public Object result() {
        return null;
    }
}
