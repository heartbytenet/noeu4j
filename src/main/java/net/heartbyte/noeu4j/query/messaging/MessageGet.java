package net.heartbyte.noeu4j.query.messaging;

import net.heartbyte.noeu4j.query.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageGet extends Query {
    public MessageGet(String topicID, String listenerID) {
        this.namespace = "messaging";
        this.method    = "message.get";
        this.params    = new HashMap<>();

        this.params.put("topic.id",    topicID);
        this.params.put("listener.id", listenerID);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Map<String, Object>> result() {
        if (this.result == null)
            return null;

        if (this.result.payload == null)
            return null;

        List<Map<String, Object>> messages = (List<Map<String, Object>>) this.result.payload.get("messages");
        if (messages == null)
            return null;

        return messages;
    }

    public List<String> content() {
        ArrayList<String> result = new ArrayList<>();

        List<Map<String, Object>> l = this.result();
        if (l == null)
            return null;

        for (Map<String, Object> m : l) {
            result.add((String) m.get("data"));
        }

        return result;
    }
}
