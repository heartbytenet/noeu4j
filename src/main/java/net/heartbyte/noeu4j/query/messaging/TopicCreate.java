package net.heartbyte.noeu4j.query.messaging;

import net.heartbyte.noeu4j.query.Query;

import java.util.HashMap;

public class TopicCreate extends Query {
    public TopicCreate(String topicID) {
        this.namespace = "messaging";
        this.method    = "topic.create";
        this.params    = new HashMap<>();

        this.params.put("topic.id", topicID);
    }
}
