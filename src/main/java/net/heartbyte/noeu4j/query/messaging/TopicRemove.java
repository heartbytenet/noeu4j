package net.heartbyte.noeu4j.query.messaging;

import net.heartbyte.noeu4j.query.Query;

import java.util.HashMap;

public class TopicRemove extends Query {
    public TopicRemove(String topicID) {
        this.namespace = "messaging";
        this.method    = "topic.remove";
        this.params    = new HashMap<>();

        this.params.put("topic.id", topicID);
    }
}
