package net.heartbyte.noeu4j.query.messaging;

import net.heartbyte.noeu4j.query.Query;

import java.util.HashMap;
import java.util.List;

public class MessageAdd extends Query {
    public MessageAdd(String topicID, String messageData, long messageTTL) {
        this.namespace = "messaging";
        this.method    = "message.add";
        this.params    = new HashMap<>();

        this.params.put("topic.id",     topicID);
        this.params.put("message.data", messageData);
        this.params.put("message.ttl",  messageTTL);
    }
}
