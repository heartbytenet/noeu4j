package net.heartbyte.noeu4j.query.misc;

import net.heartbyte.noeu4j.query.Query;

import java.util.HashMap;

public class Ping extends Query {
    public Ping(boolean sleep) {
        this.namespace = "misc";
        this.method    = "ping";
        this.params    = new HashMap<>();

        this.params.put("sleep", sleep);
    }

    @Override
    public Integer result() {
        if (this.result == null)
            return null;

        return (Integer) this.result.payload.get("pong");
    }
}
